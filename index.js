const midi = require('midi')
const {
	logAvailablePorts,
	exitIfNoPorts,
	messageHandler
} = require('require-dir')('./lib')

const input = new midi.input()

logAvailablePorts(input)
exitIfNoPorts(input)

const port = 0
input.openPort(port)
console.log(`listening on port ${port}`)

// {SysEx, Timing, Active Sensing}
input.ignoreTypes(false, false, false)

let soundId = 0
input.on('message', function(delta, message) {
	messageHandler(delta, message, soundId++)
})
