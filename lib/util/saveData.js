const fs = require('fs')
const path = require('path')

module.exports = function saveData(data, soundId) {
	const name = getNameFromId(soundId)
	const dir = path.join(__dirname, '..', '..', 'out', 'sounds')
	const filename = path.join(dir, `${name}.json`)

	// eslint-disable-next-line no-sync
	fs.writeFileSync(filename, JSON.stringify(data, null, 4))
}

const bankSize = 128

function getNameFromId(id) {
	const n = (id % bankSize) + 1
	const length = n.toString().length
	const bank = getBank(id)

	if (length === 1) return `${bank}00${n}`
	if (length === 2) return `${bank}0${n}`
	if (length === 3) return `${bank}${n}`
}

function getBank(id) {
	const bankIndex = (id / bankSize) | 0
	return String.fromCharCode(65 + bankIndex)
}
