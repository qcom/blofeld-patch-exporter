const saveData = require('./util/saveData')

module.exports = function messageHandler(delta, message, soundId) {
	const [cmd] = message

	switch (cmd) {
		case 0xF0: {
			console.log('received SysEx')
			const data = message.slice(1) // trim command from payload
			saveData(data, soundId)
			break
		}
		default: {
			console.log(`unhandled command ${cmd}`)
		}
	}
}
