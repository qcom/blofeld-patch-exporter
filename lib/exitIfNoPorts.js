module.exports = function exitIfNoPorts(input) {
	const portCount = input.getPortCount()

	if (portCount === 0) {
		console.error('no midi ports available')
		// eslint-disable-next-line no-process-exit
		process.exit(1)
	}
}
