module.exports = function logAvailablePorts(input) {
	const portCount = input.getPortCount()

	console.log('available ports:')
	for (let i = 0; i < portCount; i++) {
		const portName = input.getPortName(i)
		console.log(`[${i}] ${portName}`)
	}
	console.log('')
}
